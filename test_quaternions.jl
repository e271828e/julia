module TestQuaternions

using Reexport
using StaticArrays

using Revise
includet("quaternions.jl") #from Revise

@reexport using Main.Quaternions: Quat, UnitQuat, AbstractQuat
@reexport using Main.Quaternions: Quat16, Quat32, Quat64
@reexport using Main.Quaternions: UnitQuat16, UnitQuat32, UnitQuat64

export run_tests
export test_const, test_basics, test_upcasting, test_inner_operators,
test_promotion, test_mixed_operators

function test_const(::Type{T}) where {T}

    println("\nTesting constructors for type $T")

    T{Float32}() |> println
    T() |> println #defaults to Float64

    T{Float32}([1, 2, 3, 4]) |> println
    T(Vector{Float32}([1, 2, 3, 4])) |> println

    T{Float16}(1) |> println
    T(Float16(1)) |> println

    T{Float16}(T{Float32}()) |> println
    T(T{Float32}()) |> println

    T{Float16}(real = Float64(6), imag = Vector{Float32}([1., 2, 3])) |> println
    T(real = Float32(6), imag = Vector{Float16}([1, 2, 3])) |> println #defaults to Float64

    T{Float32}(real = Float16(6)) |> println
    T(real = Float16(6)) |> println #defaults to Float64

    T{Float16}(imag = Vector{Float16}([1, 2, 3])) |> println
    T(imag = Vector{Float16}([1, 2, 3])) |> println #defaults to Float64

    return nothing
end

function test_basics(::Type{T}) where T
    v = rand(Float64, 4)
    q = T(v)
    println(q[:])
    println(q[begin])
    println(q[end])
    println(q[2:4])
    println(q.real)
    println(q.imag)
    println(normalize!(q))
    println(copy(q)) #make sure that changing the copy does not change q
end

function test_upcasting(::Type{T}) where T
    println("\nTesting upcasting between parameter types for type $T")
    q64 = T(rand(Float64, 4))
    q32 = T(rand(Float32, 4))
    q16 = T(rand(Float16, 4))
    println(typeof(promote(q16, q32)))
    println(typeof(promote(q16, q64)))
    println(typeof(promote(q32, q64)))
end

function test_inner_operators(::Type{T}) where T
    println("\nTesting inner operators for type $T")
    q64 = T(rand(Float64, 4))
    q32 = T(rand(Float32, 4))
    q16 = T(rand(Float16, 4))

    println(q16 * q64)
    println(q64 * q64)
end

function test_promotion(::Type{T1}, ::Type{T2}) where {T1, T2}
    println("\nTesting promotion for types $T1 and $T2")
    q64 = T1{Float64}(1)
    q32 = T1{Float32}(1)
    q16 = T1{Float16}(1)
    u64 = T2{Float64}(1)
    u32 = T2{Float32}(1)
    u16 = T2{Float16}(1)
    println(promote(q16, u16))
    println(promote(q32, u32))
    println(promote(q64, u64))
    println(promote(q32, u64))
    println(promote(q64, u32))
    println(promote(q32, u16))
    println(promote(q16, u32))
end

function test_mixed_operators(::Type{T1}, ::Type{T2}) where {T1, T2}
    println("\nTesting mixed operators for types $T1 and $T2")
    q64 = T1(rand(Float64, 4))
    q32 = T1(rand(Float32, 4))
    q16 = T1(rand(Float16, 4))
    u64 = T2(rand(Float64, 4))
    u32 = T2(rand(Float32, 4))
    u16 = T2(rand(Float16, 4))
    println(q16 * u32)
    println(u16 * q64)
    println(u32 * q16)
    println(u64 * q64)
end

function run_tests()

    test_const(Quat)
    test_upcasting(Quat)
    test_inner_operators(Quat)

    test_const(UnitQuat)
    test_upcasting(UnitQuat)
    test_inner_operators(UnitQuat)

    test_promotion(Quat, UnitQuat)
    test_mixed_operators(Quat, UnitQuat)
end

end #module