using Revise

includet("test_quaternions.jl")
using Main.TestQuaternions

@code_warntype test_const(Quat)
@code_warntype test_upcasting(Quat)
@code_warntype test_inner_operators(Quat)
@code_warntype test_const(UnitQuat)
@code_warntype test_upcasting(UnitQuat)
@code_warntype test_inner_operators(UnitQuat)
@code_warntype test_promotion(Quat, UnitQuat)
@code_warntype test_mixed_operators(Quat, UnitQuat)

run_tests()